<?php

/**
 * Model resource class for issue model.
 *
 * @category   Nudorm
 * @package    ImageCheck
 * @author     Haydar Ciftci <haydar.ciftci@nudorm.com>
 */
class Nudorm_ImageCheck_Model_Resource_Issue extends Mage_Core_Model_Resource_Db_Abstract
{

    protected function _construct()
    {
        $this->_init('nudorm_imagecheck/issue', 'id');
    }

}