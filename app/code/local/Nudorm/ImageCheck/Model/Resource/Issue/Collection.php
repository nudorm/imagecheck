<?php

/**
 * Collection class for issue model.
 *
 * @category   Nudorm
 * @package    ImageCheck
 * @author     Haydar Ciftci <haydar.ciftci@nudorm.com>
 */
class Nudorm_ImageCheck_Model_Resource_Issue_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('nudorm_imagecheck/issue');
    }

    /**
     * @param mixed $store
     * @return $this
     */
    public function setStore($store)
    {
        if (is_a($store, 'Mage_Core_Model_Store')) {
            $this->addFieldToFilter('store_id', $store->getId());
        } else if (intval($store) > 0) {
            $this->addFieldToFilter('store_id', intval($store));
        } else {
            throw new InvalidArgumentException('Provided store is invalid');
        }

        return $this;
    }
}