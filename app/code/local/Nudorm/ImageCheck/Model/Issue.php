<?php

/**
 * Model for storing issues related to product images.
 *
 * @category   Nudorm
 * @package    ImageCheck
 * @author     Haydar Ciftci <haydar.ciftci@nudorm.com>
 *
 * @method integer getId()
 * @method Nudorm_ImageCheck_Model_Issue setId(integer $id)
 * @method integer getStoreId()
 * @method Nudorm_ImageCheck_Model_Issue setStoreId(integer $id)
 * @method integer getProductId()
 * @method Nudorm_ImageCheck_Model_Issue setProductId(integer $id)
 * @method string getSku()
 * @method Nudorm_ImageCheck_Model_Issue setSku(string $sku)
 * @method boolean getBaseImage()
 * @method Nudorm_ImageCheck_Model_Issue setBaseImage(boolean $hasBaseImage)
 * @method boolean getSmallImage()
 * @method Nudorm_ImageCheck_Model_Issue setSmallImage(boolean $hasSmallImage)g
 * @method boolean getThumbImage()
 * @method Nudorm_ImageCheck_Model_Issue setThumbImage(boolean $hasThumbnailImage)
 * @method boolean getDuplicatesDetected()
 * @method Nudorm_ImageCheck_Model_Issue setDuplicatesDetected(boolean $duplicatesDetected)
 * @method string getCheckedTime()
 * @method Nudorm_ImageCheck_Model_Issue setCheckedTime(string $checkedTime)
 */
class Nudorm_ImageCheck_Model_Issue extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('nudorm_imagecheck/issue');
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return Nudorm_ImageCheck_Model_Issue
     */
    public function loadByProduct(Mage_Catalog_Model_Product $product, Mage_Core_Model_Store $store)
    {
        $collection = $this->getResourceCollection()->setStore($store)
            ->setPageSize(1)->setCurPage(1)->addFieldToFilter('product_id', $product->getId());

        /** @var Nudorm_ImageCheck_Model_Issue $object */
        foreach ($collection as $object) {
            return $object;
        }

        /** @var Nudorm_ImageCheck_Model_Issue $issue */
        $issue = Mage::getModel('nudorm_imagecheck/issue');
        $issue->setProductId($product->getId());
        $issue->setSku($product->getSku());

        return $issue;
    }

}