<?php

/**
 * Backend controller class for issues management.
 *
 * @category   Nudorm
 * @package    ImageCheck
 * @author     Haydar Ciftci <haydar.ciftci@nudorm.com>
 */
class Nudorm_ImageCheck_Adminhtml_Imagecheck_IssuesController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('nudorm_imagecheck/grid_issues'));
        $this->renderLayout();
    }

    public function exportCsvAction()
    {
        $fileName = 'Issue_export.csv';
        $content = $this->getLayout()->createBlock('nudorm_imagecheck/grid_issues_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportExcelAction()
    {
        $fileName = 'Issue_export.xml';
        $content = $this->getLayout()->createBlock('nudorm_imagecheck/grid_issues_grid')->getExcel();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select Issue(s).'));
        } else {
            try {
                foreach ($ids as $id) {
                    $model = Mage::getSingleton('nudorm_imagecheck/issue')->load($id);
                    $model->delete();
                }

                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been deleted.', count($ids))
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('nudorm_imagecheck')->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massFixAction()
    {
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select Issue(s).'));
        } else {
            try {
                foreach ($ids as $id) {
                    $model = Mage::getSingleton('nudorm_imagecheck/issue')->load($id);
                    $result = Mage::helper('nudorm_imagecheck')->fixProductImages($model);

                    if ($result) {
                        $model->delete();
                    }
                }

                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been fixed and deleted.', count($ids))
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('nudorm_imagecheck')->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('nudorm_imagecheck/issue');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->_getSession()->addError(
                    Mage::helper('nudorm_imagecheck')->__('This Issue no longer exists.')
                );
                $this->_redirect('*/*/');
                return;
            }
        }

        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('current_model', $model);

        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('nudorm_imagecheck/grid_issue_edit'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function fixAction()
    {
        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('id');
            $model = Mage::getModel('nudorm_imagecheck/issue');
            if ($id) {
                $model->load($id);
                if (!$model->getId()) {
                    $this->_getSession()->addError(
                        Mage::helper('nudorm_imagecheck')->__('This Issue no longer exists.')
                    );
                    $this->_redirect('*/*/index');
                    return;
                }
            }

            // save model
            try {
                $model->addData($data);
                $this->_getSession()->setFormData($data);
                $model->save();
                $this->_getSession()->setFormData(false);

                $result = Mage::helper('nudorm_imagecheck')->fixProductImages($model);

                if ($result) {
                    $model->delete();
                    $this->_getSession()->addSuccess(Mage::helper('nudorm_imagecheck')->__('The Issue has been resolved.'));
                } else {
                    $this->_getSession()->addError(Mage::helper('nudorm_imagecheck')->__('An error occurred during resolution of the Issue'));
                }
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(Mage::helper('nudorm_imagecheck')->__('Unable to resolve the Issue.'));
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                // init model and delete
                $model = Mage::getModel('nudorm_imagecheck/issue');
                $model->load($id);
                if (!$model->getId()) {
                    Mage::throwException(Mage::helper('nudorm_imagecheck')->__('Unable to find a Issue to delete.'));
                }
                $model->delete();
                // display success message
                $this->_getSession()->addSuccess(
                    Mage::helper('nudorm_imagecheck')->__('The Issue has been deleted.')
                );
                // go to grid
                $this->_redirect('*/*/index');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('nudorm_imagecheck')->__('An error occurred while deleting Issue data. Please review log and try again.')
                );
                Mage::logException($e);
            }
            // redirect to edit form
            $this->_redirect('*/*/edit', array('id' => $id));
            return;
        }
// display error message
        $this->_getSession()->addError(
            Mage::helper('nudorm_imagecheck')->__('Unable to find a Issue to delete.')
        );
// go to grid
        $this->_redirect('*/*/index');
    }
}