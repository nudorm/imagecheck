<?php
/**
 * Setup script to create the issues table.
 *
 * @category   Nudorm
 * @package    ImageCheck
 * @author     Haydar Ciftci <haydar.ciftci@nudorm.com>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
$installer->getConnection()->addColumn(
    $installer->getTable('nudorm_imagecheck/issue'), 'store_id', array(
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'nullable' => false,
    'unsigned' => true,
    'comment' => 'Store view'
));


$installer->getConnection()->dropForeignKey(
    $installer->getTable('nudorm_imagecheck/issue'),
    $installer->getFkName($installer->getTable('nudorm_imagecheck/issue'), 'product_id', $installer->getTable('catalog/product'), 'entity_id')
);

$installer->getConnection()->dropIndex(
    $installer->getTable('nudorm_imagecheck/issue'),
    $installer->getIdxName($installer->getTable('nudorm_imagecheck/issue'), 'product_id',
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
);

$installer->getConnection()->addIndex(
    $installer->getTable('nudorm_imagecheck/issue'),
    $installer->getIdxName(
        $installer->getTable('nudorm_imagecheck/issue'),
        array('store_id', 'product_id'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    ),
    array('store_id', 'product_id'),
    Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
);

$installer->getConnection()->addForeignKey(
    $installer->getFkName(
        $installer->getTable('nudorm_imagecheck/issue'),
        'store_id',
        $installer->getTable('core/store'),
        'store_id'
    ), //$fkName
    $installer->getTable('nudorm_imagecheck/issue'), //$tableName
    'store_id', //$columnName
    $installer->getTable('core/store'), //$refTableName
    'store_id' //$refColumnName
);

$installer->getConnection()->addForeignKey(
    $installer->getFkName(
        $installer->getTable('nudorm_imagecheck/issue'),
        'product_id',
        $installer->getTable('catalog/product'),
        'entity_id'
    ),
    $installer->getTable('nudorm_imagecheck/issue'),
    'product_id',
    $installer->getTable('catalog/product'),
    'entity_id'
);

$installer->endSetup();