<?php
/**
 * Setup script to create the issues table.
 *
 * @category   Nudorm
 * @package    ImageCheck
 * @author     Haydar Ciftci <haydar.ciftci@nudorm.com>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
$table = $installer->getConnection()->newTable($installer->getTable('nudorm_imagecheck/issue'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
    ), 'Id')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Product ID')
    ->addColumn('sku', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Product ID')
    ->addColumn('base_image', Varien_Db_Ddl_Table::TYPE_BOOLEAN, 1, array(
        'nullable' => false,
        'default' => '0',
    ), 'Baseimage available')
    ->addColumn('small_image', Varien_Db_Ddl_Table::TYPE_BOOLEAN, 1, array(
        'nullable' => false,
        'default' => '0',
    ), 'Smallimage available')
    ->addColumn('thumb_image', Varien_Db_Ddl_Table::TYPE_BOOLEAN, 1, array(
        'nullable' => false,
        'default' => '0',
    ), 'Thumbnail image available')
    ->addColumn('duplicates_detected', Varien_Db_Ddl_Table::TYPE_BOOLEAN, 1, array(
        'nullable' => false,
        'default' => '0',
    ), 'Duplicates detected')
    ->addColumn('checked_time', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => true,
        'default' => null,
    ), 'Checked Date/Time')
    ->addIndex(
        $installer->getIdxName('nudorm_imagecheck/issue', 'product_id', Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
        'product_id',
        array(
            'type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        )
    )
    ->addForeignKey(
        $installer->getFkName('nudorm_imagecheck/issue', 'product_id', 'catalog/product', 'entity_id'),
        'product_id',
        $installer->getTable('catalog/product'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );

$installer->getConnection()->createTable($table);
$installer->endSetup();