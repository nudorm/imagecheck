<?php

/**
 * Container block class for the issues grid in the adminhtml.
 *
 * @category   Nudorm
 * @package    ImageCheck
 * @author     Haydar Ciftci <haydar.ciftci@nudorm.com>
 */
class Nudorm_ImageCheck_Block_Grid_Issues extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'nudorm_imagecheck';
        $this->_controller = 'grid_issues';
        $this->_headerText = $this->__('Issues');
        parent::__construct();
        $this->removeButton('add');
    }
}

