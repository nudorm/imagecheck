<?php

/**
 * Block class for the adminhtml grid widget.
 *
 * @category   Nudorm
 * @package    ImageCheck
 * @author     Haydar Ciftci <haydar.ciftci@nudorm.com>
 */
class Nudorm_ImageCheck_Block_Grid_Issues_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('grid_id');
        // $this->setDefaultSort('COLUMN_ID');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('nudorm_imagecheck/issue')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {


        $this->addColumn('id', array(
            'header' => $this->__('ID'),
            'index' => 'id',
            'width' => '50px'
        ))->addColumn('checked_time', array(
            'header' => $this->__('Checked time'),
            'index' => 'checked_time',
            'type' => 'datetime',
            'width' => '200px'
        ))->addColumn('store_id', array(
            'header' => $this->__('Store'),
            'index' => 'store_id',
            'type' => 'store',
            'store_view' => true,
            'display_deleted' => true,
        ))->addColumn('product_sku', array(
            'header' => $this->__('SKU'),
            'index' => 'sku',
            'type' => 'text'
        ))->addColumn('duplicates_detected', array(
            'header' => $this->__('Duplicates found'),
            'index' => 'duplicates_detected',
            'type' => 'options',
            'options' => array(
                Mage::helper('adminhtml')->__('No'),
                Mage::helper('adminhtml')->__('Yes')
            ),
            'renderer' => 'Nudorm_ImageCheck_Block_Widget_Grid_Column_Renderer_Boolean',
            'width' => '100px'
        ))->addColumn('base_image', array(
            'header' => $this->__('Base image set'),
            'index' => 'base_image',
            'type' => 'options',
            'options' => array(
                Mage::helper('adminhtml')->__('No'),
                Mage::helper('adminhtml')->__('Yes')
            ),
            'renderer' => 'Nudorm_ImageCheck_Block_Widget_Grid_Column_Renderer_Boolean',
            'width' => '100px'
        ))->addColumn('small_image', array(
            'header' => $this->__('Small image set'),
            'index' => 'small_image',
            'type' => 'options',
            'options' => array(
                Mage::helper('adminhtml')->__('No'),
                Mage::helper('adminhtml')->__('Yes')
            ),
            'renderer' => 'Nudorm_ImageCheck_Block_Widget_Grid_Column_Renderer_Boolean',
            'width' => '100px'

        ))->addColumn('thumb_image', array(
            'header' => $this->__('Thumbnail image set'),
            'index' => 'thumb_image',
            'type' => 'options',
            'options' => array(
                Mage::helper('adminhtml')->__('No'),
                Mage::helper('adminhtml')->__('Yes')
            ),
            'renderer' => 'Nudorm_ImageCheck_Block_Widget_Grid_Column_Renderer_Boolean',
            'width' => '100px'
        ));

        $this->addExportType('*/*/exportCsv', $this->__('CSV'));

        $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        $modelPk = Mage::getModel('nudorm_imagecheck/issue')->getResource()->getIdFieldName();
        $this->setMassactionIdField($modelPk);
        $this->getMassactionBlock()->setFormFieldName('ids');
        // $this->getMassactionBlock()->setUseSelectAll(false);
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));
        $this->getMassactionBlock()->addItem('fix', array(
            'label' => $this->__('Fix and delete'),
            'url' => $this->getUrl('*/*/massFix'),
        ));
        return $this;
    }
}
