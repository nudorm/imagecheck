<?php

/**
 * Form container for the issue editing.
 *
 * @category   Nudorm
 * @package    ImageCheck
 * @author     Haydar Ciftci <haydar.ciftci@nudorm.com>
 */
class Nudorm_ImageCheck_Block_Grid_Issue_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        // $this->_objectId = 'id';
        parent::__construct();
        $this->_blockGroup = 'nudorm_imagecheck';
        $this->_controller = 'grid_issue';
        $this->_mode = 'edit';
        $modelTitle = $this->_getModelTitle();
//        $this->_updateButton('save', 'label', $this->_getHelper()->__("Save $modelTitle"));
//        $this->_addButton('fixanddelete', array(
//            'label' => $this->_getHelper()->__('Fix and delete Edit'),
//            'onclick' => 'fixanddeleteEdit()',
//            'class' => 'save',
//        ), -100);

        $this->removeButton('save');
        $this->removeButton('reset');

        $this->_addButton('fixanddelete', array(
            'label' => $this->_getHelper()->__('Fix and Delete'),
            'onclick' => 'fixAndDelete()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
            function fixAndDelete(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    protected function _getHelper()
    {
        return Mage::helper('nudorm_imagecheck');
    }

    protected function _getModel()
    {
        return Mage::registry('current_model');
    }

    protected function _getModelTitle()
    {
        return 'Issue';
    }

    public function getHeaderText()
    {
        $model = $this->_getModel();
        $modelTitle = $this->_getModelTitle();
        if ($model && $model->getId()) {
            return $this->_getHelper()->__("Edit $modelTitle (ID: {$model->getId()})");
        } else {
            return $this->_getHelper()->__("New $modelTitle");
        }
    }


    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/*/index');
    }

    public function getDeleteUrl()
    {
        return $this->getUrl('*/*/delete', array($this->_objectId => $this->getRequest()->getParam($this->_objectId)));
    }
}
