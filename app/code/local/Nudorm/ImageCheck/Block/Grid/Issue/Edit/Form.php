<?php

/**
 * Form widget for editing an issue.
 *
 * @category   Nudorm
 * @package    ImageCheck
 * @author     Haydar Ciftci <haydar.ciftci@nudorm.com>
 */
class Nudorm_ImageCheck_Block_Grid_Issue_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _getModel()
    {
        return Mage::registry('current_model');
    }

    protected function _getHelper()
    {
        return Mage::helper('nudorm_imagecheck');
    }

    protected function _getModelTitle()
    {
        return 'Issue';
    }

    protected function _prepareForm()
    {
        $model = $this->_getModel();
        $modelTitle = $this->_getModelTitle();
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/fix'),
            'method' => 'post'
        ));

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend' => $this->_getHelper()->__("$modelTitle Information"),
            'class' => 'fieldset-wide',
        ));

        $fieldset->addType('boolean', 'Nudorm_ImageCheck_Block_Adminhtml_Issue_Renderer_Boolean');

        if ($model && $model->getId()) {
            $modelPk = $model->getResource()->getIdFieldName();
            $fieldset->addField($modelPk, 'hidden', array(
                'name' => 'id',
            ));
        }

        $fieldset->addField('store_id', 'label', array(
            'name' => 'store_id',
            'label' => $this->_getHelper()->__('Store'),
            'title' => $this->_getHelper()->__('Store Id'),

        ));

        $fieldset->addField('product_id', 'label', array(
            'name' => 'product_id',
            'label' => $this->_getHelper()->__('Product Id'),
            'title' => $this->_getHelper()->__('Record identifier of the affected product'),

        ));

        $fieldset->addField('sku', 'label', array(
            'name' => 'sku',
            'label' => $this->_getHelper()->__('Product SKU'),
            'title' => $this->_getHelper()->__('SKU of the affected product'),

        ));
        $fieldset->addField('base_image', 'boolean', array(
            'name' => 'base_image',
            'label' => $this->_getHelper()->__('Base Image'),
            'title' => $this->_getHelper()->__('Indicates if the product has a base image set'),
        ));
        $fieldset->addField('small_image', 'boolean', array(
            'name' => 'small_image',
            'label' => $this->_getHelper()->__('Small Image'),
            'title' => $this->_getHelper()->__('Indicates if the product has a small image set'),

        ));
        $fieldset->addField('thumb_image', 'boolean', array(
            'name' => 'thumb_image',
            'label' => $this->_getHelper()->__('Thumbnail Image'),
            'title' => $this->_getHelper()->__('Indicates if the product has a thumbnail image set'),

        ));
        $fieldset->addField('duplicates_detected', 'boolean', array(
            'name' => 'duplicates_detected',
            'label' => $this->_getHelper()->__('Duplicates detected'),
            'title' => $this->_getHelper()->__('Indicates if the product has duplicates in the images list'),

        ));
        $fieldset->addField('checked_time', 'label', array(
            'name' => 'checked_time',
            'label' => $this->_getHelper()->__('Checked time'),
            'title' => $this->_getHelper()->__('Indicates the when was this check performed.'),

        ));

        if ($model) {
            $form->setValues($model->getData());
        }
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
