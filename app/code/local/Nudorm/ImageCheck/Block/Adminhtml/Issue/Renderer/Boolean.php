<?php

/**
 * @category   Nudorm
 * @package    ImageCheck
 * @author     Haydar Ciftci <haydar.ciftci@nudorm.com>
 */
class Nudorm_ImageCheck_Block_Adminhtml_Issue_Renderer_Boolean extends Varien_Data_Form_Element_Abstract
{
    protected $_element;

    public function getElementHtml()
    {
        $value = intval($this->getEscapedValue()) == 1 ? 'Yes' : 'No';

        $html = $value;
        return $html;
    }
}