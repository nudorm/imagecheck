<?php

/**
 * Adminhtml grid column renderer for boolean values.
 *
 * @category   Nudorm
 * @package    ImageCheck
 * @author     Haydar Ciftci <haydar.ciftci@nudorm.com>
 */
class Nudorm_ImageCheck_Block_Widget_Grid_Column_Renderer_Boolean extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        $result = intval($value) === 1 ? 'Yes' : 'No';
        return $result;
    }
}