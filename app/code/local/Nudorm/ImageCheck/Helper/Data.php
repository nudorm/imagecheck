<?php

/**
 * Helper class with various methods.
 *
 * @category   Nudorm
 * @package    ImageCheck
 * @author     Haydar Ciftci <haydar.ciftci@nudorm.com>
 */
class Nudorm_ImageCheck_Helper_Data extends Mage_Core_Helper_Abstract
{
    const NOT_SET = 'no_selection';

    public function analyzeCatalogProducts() {

        Mage::log('Start of Image Check run');
        /** @var Nudorm_ImageCheck_Helper_Data $helper */
        $storeCollection = Mage::getModel('core/store')->getCollection();

        /** @var Mage_Core_Model_Store $store */
        foreach ($storeCollection as $store) {
            Mage::log('Running for store ' . $store->getCode());
            Mage::app()->setCurrentStore($store->getCode());

            /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
            $collection = Mage::getModel('catalog/product')->getCollection()
                ->setStoreId($store->getId())
                ->addAttributeToSelect('image')
                ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('thumbnail')
                ->load();

            Mage::log('Found ' . $collection->count() . ' products for store ' . $store->getCode());

            /** @var Mage_Catalog_Model_Product $product */
            foreach ($collection as $product) {
                /** @var Nudorm_ImageCheck_Model_Issue $issue */
                $issue = $this->analyzeProductImages($product, $store);
                if (!$issue->getBaseImage() ||
                    !$issue->getSmallImage() ||
                    !$issue->getThumbImage() ||
                    $issue->getDuplicatesDetected()
                ) {
                    // only save if there was an issue
                    $issue->setStoreId($store->getId());
                    $issue->save();
                }
            }
        }

        Mage::log('End of Image Check run');
    }

    /**
     * Go trough base image settings and look for duplicates.
     *
     * @param Mage_Catalog_Model_Product $product
     * @param Mage_Core_Model_Store $store
     * @return Nudorm_ImageCheck_Model_Issue
     */
    public function analyzeProductImages(Mage_Catalog_Model_Product $product, Mage_Core_Model_Store $store)
    {
        $mediaApi = Mage::getSingleton('catalog/product_attribute_media_api');
        $issue = Mage::getModel('nudorm_imagecheck/issue')->loadByProduct($product, $store);
        $md5array = array();
        $mediaItems = $mediaApi->items($product->getId());

        foreach ($mediaItems as $item) {
            $imagePath = Mage::getBaseDir('media') . DS . 'catalog/product' . $item['file'];

            if (is_readable($imagePath)) {
                $md5file = md5_file($imagePath);

                if (in_array($md5file, $md5array)) {
                    $issue->setDuplicatesDetected(true);
                    break;
                }

                $md5array[] = $md5file;
            }
        }

        $issue->setProductId($product->getId())
            ->setSku($product->getSku())
            ->setBaseImage(($product->getImage() != self::NOT_SET))
            ->setSmallImage(($product->getSmallImage() != self::NOT_SET))
            ->setThumbImage(($product->getThumbnail() != self::NOT_SET))
            ->setCheckedTime(Mage::getModel('core/date')->date());

        return $issue;
    }

    /**
     * Try to fix the image issues.
     *
     * @param Nudorm_ImageCheck_Model_Issue $issue
     * @return bool
     */

    public function fixProductImages(Nudorm_ImageCheck_Model_Issue $issue)
    {
        Mage::app()->setCurrentStore(Mage::getModel('core/store')->load($issue->getStoreId()));

        $product = Mage::getModel('catalog/product')->load($issue->getProductId());
        $mediaApi = Mage::getSingleton('catalog/product_attribute_media_api');
        $mediaItems = $mediaApi->items($product->getId());
        $md5array = array();
        $newBaseItem = null;

        if (count($mediaItems) > 0) {
            $newBaseItem = $mediaItems[0]['file'];
        }

        if ($product->getImage() == self::NOT_SET) {
            $product->setImage($newBaseItem);
        } else {
            $newBaseItem = $product->getImage();
        }
        if ($product->getSmallImage() == self::NOT_SET) {
            $product->setSmallImage($newBaseItem);
        }
        if ($product->getThumbnail() == self::NOT_SET) {
            $product->setThumbnail($newBaseItem);
        }

        try {
            $product->save();
        } catch (Exception $e) {
            Mage::logException($e);
            return false;
        }

        $md5array[] = md5_file($imagePath = Mage::getBaseDir('media') . DS . 'catalog/product' . $newBaseItem);

        foreach ($mediaItems as $item) {

            $imagePath = Mage::getBaseDir('media') . DS . 'catalog/product' . $item['file'];

            if (is_readable($imagePath)) {
                $md5file = md5_file($imagePath);

                if (in_array($md5file, $md5array)) {
                    try {
                        $mediaApi->remove($product->getId(), $item['file']);
                    } catch (Exception $e) {
                        Mage::logException($e);
                        return false;
                    }
                } else {
                    $md5array[] = $md5file;
                }
            }
        }
        return true;
    }
}