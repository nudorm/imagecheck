<?php
require 'abstract.php';

/**
 * Shell file for running the check manually.
 *
 * @category   Nudorm
 * @package    ImageCheck
 * @author     Haydar Ciftci <haydar.ciftci@nudorm.com>
 */
class Nudorm_ImageCheck_Shell extends Mage_Shell_Abstract
{
    /**
     * Run script
     */
    public function run()
    {
        $mode = $this->getArg('mode');
        switch ($mode) {
            case 'check':
                $this->checkOperation();
                break;
            case 'fix':
                $this->fixOperation();
                break;
            default:
                die($this->printUsage());
        }
    }

    protected function printUsage()
    {
        return <<<USAGE
Usage:  php -f imagecheck.php -- [options]

  --mode <mode>         Run in mode: [check|fix]
  help                  This help

USAGE;
    }

    /**
     * Try to fix issues reported by the check operation.
     */
    public function fixOperation()
    {
        $storeCollection = Mage::getModel('core/store')->getCollection();

        /** @var Mage_Core_Model_Store $store */
        foreach ($storeCollection as $store) {
            Mage::log('Running for store ' . $store->getCode());
            Mage::app()->setCurrentStore($store->getCode());

            $issues = Mage::getModel('nudorm_imagecheck/issue')->getCollection()->setStore($store);

            foreach ($issues as $issue) {
                try {
                    if (Mage::helper('nudorm_imagecheck')->fixProductImages($issue)) {
                        $issue->delete();
                    }

                } catch (Exception $e) {
                    echo $e->getMessage();
                    Mage::logException($e);
                }
            }
        }
    }

    /**
     * Run the check operation to generate a list of issues with product pictures.
     */
    public function checkOperation()
    {
        try {
            Mage::helper('nudorm_imagecheck')->analyzeCatalogProducts();
        } catch (Exception $e) {
            echo $e->getMessage();
            Mage::logException($e);
        }
    }
}

$imageCheck = new Nudorm_ImageCheck_Shell();
$imageCheck->run();